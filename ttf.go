// Autor: Tomáš Vlček <tvlcek@mail.muni.cz>
// Licence: Mozilla Public License 2.0

package ttf

import (
	"errors"
	"regexp"
)

// Odstraní ze řetězce všechny nevhodné znaky — interpunkci, znaky s diakritikou atp.
func CleanString(unclean_string string) string {
	var re *regexp.Regexp = regexp.MustCompile("[^a-zA-Z0-9]+")
	var alphanumeric_string string = re.ReplaceAllString(unclean_string, "")
	return alphanumeric_string
}

// Funkce rozdělí slovo »word« na jednotlivá písmena abecedy »alphabet«. Písmena se smějí
// skládat z více znaků. Pokud se ve slově vyskytne neznámé písmeno, funkce vrátí chybu.
func TokenizeIntoSubwords(word string, alphabet []string) ([]string, error) {
	var subwords []string
	word_in_runes := []rune(word)
	var i int = 0
	for i < len(word_in_runes) {
		var found bool = false
		for _, letter := range alphabet {
			lelen := len([]rune(letter)) // délka písmena v runách
			if len(word_in_runes)-i >= lelen && string(word_in_runes[i:i+lelen]) == letter {
				subwords = append(subwords, letter)
				i += lelen
				found = true
				break
			}
		}
		if !found {
			// subwords = append(subwords, string(word_in_runes[i]))
			// i = i + 1
			return nil, errors.New("unknown letter")
		}
	}
	return subwords, nil
}
